import React, { useState } from "react";
import { ethers } from "ethers";
import "./App.css";

// Import ABI Code to interact with Smart Contract
import SimpleCounter from "./artifacts/contracts/SimpleCounter.sol/Counter.json";

// The contract address
const counterAddress = "0x5FbDB2315678afecb367f032d93F642f64180aa3";

function App() {

  // Property Variables
  const [counter, setCount] = useState(0); // useState returns a pair. 'count' is the current state. 'setCount' is a function we can use to update the state.


  // Request access to the user's Meta Mask Account
  async function requestAccount() {
    await window.ethereum.request( { method: 'eth_requestAccounts'});
  }

  // Fetches the current value store in increment
  async function incrementButton() {    

    // If MetaMask exists
    if (typeof window.ethereum !== 'undefined') {
      await requestAccount(); // requestAccount = Metamask conenct

      setCount(function (count) {
        return (count += 1);
      });

      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const contract = new ethers.Contract(counterAddress, SimpleCounter.abi, provider);
      try {
        // Call SimpleCounter.getCount()
        /*
          function getCount() public view returns (uint256) {
            return count;
          }
        */
        const data = await contract.getCount();
        console.log("data: ", data);
      } catch (error) {
        console.log('Error: ', error);
      }
    }
  }


  async function decrementButton() {
    if (!counter) return;

    // If MetaMask exists
    if (typeof window.ethereum !== 'undefined') {


      setCount(function (count) {
        if (count > 0) {
          return (count -= 1);
        } else {
          return (count = 0);
        }
      });

      const provider = new ethers.providers.Web3Provider(window.ethereum);
      const signer = provider.getSigner();

      // Create contract with signer
      const contract = new ethers.Contract(counterAddress, SimpleCounter.abi, signer);
      const transaction = await contract.getCount(counter);
    }
  }

  

  return (
    <div className="App">
      <div className="App-header">
        <div>
          <h2>SimpleCounter.sol</h2>
          <h1>{counter}</h1>
        </div>
        <div className="custom-buttons">
          <button onClick={incrementButton}>Increment</button>
          <button onClick={decrementButton}>Decrement</button>
        </div>
      </div>
    </div>
  );
}

export default App;
