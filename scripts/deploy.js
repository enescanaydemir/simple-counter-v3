const hre = require("hardhat");

async function main() {
  const SimpleCounter = await hre.ethers.getContractFactory("Counter");
  const simpleCounter = await SimpleCounter.deploy();

  await simpleCounter.deployed();

  console.log("SimpleCounter deployed to:", simpleCounter.address);
}
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
